Read.me

This repository contains scripts and raw data for the analysis performed in the manuscript entitled "Different approaches to processing environmental DNA samples in turbid waters have distinct effects for fish, bacterial and archaea communities: https://doi.org/10.24072/pci.ecology.100427".

The raw data contains only the dataset pertaining to the Topanga Lagoon. It does not include the full MiSeq run that was used for correcting contamination from tag-jumps in the MetabaR step.
