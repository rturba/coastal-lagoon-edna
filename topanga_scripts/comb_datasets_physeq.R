#Date: 26-Jan-20
#Combining tables from both runs

library(ranacapa)
library(phyloseq)
library(vegan)
library(ggplot2)
library(dplyr)
library(tidyverse)


#combining tables

primer <- "16S"
month1 <- "nov"
month2 <- "jul"

input_biom_path_r1 <- paste0("contamination_pipeline/step2_gruinard/Output_csv/",month1,"_run/post_occupancy_results_sum.taxonomy_tech_reps_separate_read_counts_",month1,"_",primer,".csv")
input_biom_path_r2 <- paste0("contamination_pipeline/step2_gruinard/Output_csv/",month2,"_run/post_occupancy_results_sum.taxonomy_tech_reps_separate_read_counts_",month2,"_",primer,".csv")
input_meta_path <- "contamination_pipeline/topanga_meta/topanga_meta.txt"

biom_r1 <- read.csv(input_biom_path_r1, header = 1, sep = ",", stringsAsFactors = F)
colnames(biom_r1)
colnames(biom_r1) <- gsub("\\.4", ".3", colnames(biom_r1))
#colnames(biom_r1) <- gsub("_51", "", colnames(biom_r1)) #<- for CO1 primer

biom_r2 <- read.csv(input_biom_path_r2, header = 1, sep = ",", stringsAsFactors = F)
colnames(biom_r2)
colnames(biom_r2) <- gsub("\\.4", ".3", colnames(biom_r2))
#colnames(biom_r2) <- gsub("_50", "", colnames(biom_r2)) #<- for CO1 primer

setdiff(colnames(biom_r1), colnames(biom_r2))

ASV_table <- bind_rows(biom_r1, biom_r2) %>%
  group_by(sum.taxonomy) %>%
  summarise_all(funs(sum(., na.rm = TRUE)))
dim(ASV_table)

colnames(ASV_table) <- gsub("X16S_", "", colnames(ASV_table)) #change for each primer

write.csv(ASV_table, file = paste0("topanga/output/",primer,"/ASV_table_comb_",primer,".csv"))

meta <- read.table(input_meta_path, header = 1, sep = "\t", stringsAsFactors = F)
dif <- setdiff(meta$Seq_number, colnames(ASV_table))
meta_edit <- meta[!(meta$Seq_number %in% c(dif)),]
meta_edit[,1]

convert_anacapa_to_phyloseq <- function (taxon_table, metadata_file) 
{
  validate_input_files(taxon_table, metadata_file)
  taxon_table2 <- group_anacapa_by_taxonomy(taxon_table) %>% 
    tibble::column_to_rownames("sum.taxonomy") %>% 
    as.matrix
  taxon_table2 <- taxon_table2[, order(colnames(taxon_table2))]
  ana_taxon_table_physeq <- phyloseq::otu_table(taxon_table2, 
                                                taxa_are_rows = TRUE)
  taxon_names <- reshape2::colsplit(rownames(taxon_table2), 
                                    ";", names = c("Superkingdom", "Phylum", "Class", "Order", 
                                                   "Family", "Genus", "Species")) %>% 
    as.matrix
  rownames(taxon_names) <- rownames(taxon_table2)
  tax_physeq <- phyloseq::tax_table(taxon_names)
  colnames(tax_physeq) <- c("Superkingdom", "Phylum", "Class", 
                            "Order", "Family", "Genus", "Species")
  physeq_object <- phyloseq::phyloseq(ana_taxon_table_physeq, 
                                      tax_physeq)
  rownames(metadata_file) <- metadata_file[, 1]
  metadata_file <- metadata_file[order(metadata_file[, 1]), 
                                 ]
  sampledata <- phyloseq::sample_data(metadata_file)
  phyloseq::merge_phyloseq(physeq_object, sampledata)
}

physeq <- convert_anacapa_to_phyloseq(ASV_table, meta_edit)

saveRDS(physeq, file = paste0("topanga/output/",primer,"/physeq_comb_",primer,".Rds"))

### Visualize Read Counts Across Samples for Barcode_1

tmp <- ASV_table  
columns <- colnames(tmp)
remove <- c("sum.taxonomy")
gathercols <-  columns[! columns %in% remove]
tmp %>% 
  pivot_longer(., cols=gathercols, names_to="sample", values_to="reads") -> tmp_long

tmp_long$reads <- as.numeric(tmp_long$reads)

tmp_long %>%
  group_by(sample) %>%
  mutate (TotalReadsperSample = sum(reads)) %>%
  arrange(desc(TotalReadsperSample)) %>%
  ggplot(., aes(x=sample, y=TotalReadsperSample)) + 
  geom_point(size = 3) +
  theme_bw() +
  ggtitle(paste0("Read Count Across Samples (",primer,")")) + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) -> plot_1

ggsave(plot=plot_1, paste0("topanga/output/",primer,"/readBySample_",primer,".png"), 
       device = "png", width = 12, height = 6, units = "in")


